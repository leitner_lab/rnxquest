from setuptools import setup, find_packages

VERSION = '1.0' 
DESCRIPTION = 'RNxQuest - an extension to xQuest for protein-RNA cross-linking'
LONG_DESCRIPTION = 'A Python package companion to xQuest, extending its features for analysis of stable isotope labelled protein-RNA cross-linking data'

# Setting up
setup(
       # the name must match the folder name 'rnxquest'
        name="rnxquest", 
        version=VERSION,
        author="Chris P. Sarnowski",
        author_email="sarnowski@imsb.biol.ethz.ch",
        entry_points = {'console_scripts': ['rnxq_paramgen = rnxquest.paramgen:main', 'rnxq_extractxml = rnxquest.extractxml:main', 'rnxq_topin = rnxquest.rnxq2pin:cmd_ln_main', 'rnxq_getfiles = rnxquest.gettemplates:main', 'rnxq_allposrna = rnxquest.allposrna:main']},
        description=DESCRIPTION,
        long_description=LONG_DESCRIPTION,
#        packages=find_packages(),
        packages=['rnxquest'],
#        package_dir={'mypkg': 'src/mypkg'},
        package_data={'rnxquest': ['templates/*']},
        include_package_data=True,
        install_requires=['lxml', 'pandas', 'fastaparser', 'matplotlib', 'numpy', 'plotly', 'scipy', 'pathlib', 'mokapot'], # add any additional packages that 
        # needs to be installed along with your package. Eg: 'caer'
        
        keywords=['xQuest', 'xquest', 'XL-MS', 'RNA'],
        classifiers= [
            "Development Status :: 5 - Production/Stable",
            "Intended Audience :: Science/Research",
            "Programming Language :: Python :: 3",
            "Operating System :: Unix",
            "Operating System :: Microsoft :: Windows",
        ]
)