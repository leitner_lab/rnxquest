"""
Initialize the rnxquest package.
"""
from pkg_resources import get_distribution, DistributionNotFound

try:
    __version__ = get_distribution(__name__).version
except DistributionNotFound:
    pass

import rnxquest.extractxml
import rnxquest.gettemplates
import rnxquest.allposrna
import rnxquest.rnxq2pin
import rnxquest.paramgen
from rnxquest.plottools import swap, permute, get_permutations, get_heat_map, update_figure, color_palette_standard, color_palette_1, split_at, get_summary_csv, remove_dupspec
from rnxquest.fdrtools import DecoyAnalysis, observed_FDR, error_plot, FDR_binning, bin_FDR_calc, tfr_FDR, mokapot_FDR