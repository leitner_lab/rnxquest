# -*- coding: utf-8 -*-
"""Created on Wed Jun 12 12:26:33 2019
Aiming to recreate the functionality of the "Generate Param files" R script, but with ability to run from the command line.

v0.896
- adds support for SDA protein-RNA XL
v0.895
- adds a new flag "-mzxml" wich must be set to "c" for .c.mzXML files, just like in pQuest of core xQuest
v0.894
- changes the definition file generating function to the rnxq specific one
- removes --extract_path argument, and replaces with hard coded reference to the extraction script via command line access command (if module is installed)
v0.893
- changes to accomodate modified .def files (no longer works with default from xQuest)
- returns to specific tags that must be set by the parameeter generation script for RNA searches
v0.892:
- converts to package format
v0.891:
- puts lab schemes back into main file
v0.89:
- adds version flag (appended to parameters output file)
- option to read loss types from new dictionary file (RNA_losses.py)
- reads labelling schemes from a module for easier editing
v0.88:
- renamed labelling schemes (genericised)
- error handling to validate command line input parameters
v0.86:
- neutral loss names replaced with atomic compositions
- RNA losses now delivered to "loss_type" instead of "type_y"
- must be used with extract_XML.py v0.2 or greater (because of change of loss_type column above)
v0.85:
- now uses the default .def files produced by runXquest.pl -getdef, and includes the ability to set a peptide only search
v0.84:
- adds new labelling scheme for AK_DEB (i.e. pre-labelled RNA with DEB XL)
v0.83:
- adds new labelling scheme (TdV_N) for nitrogen-only labelled metabolically labelled nucleotides
v0.82:
- converts the sample type when reading "samplelist" into string so numeric values cam be used 
v0.81:
- added additional mass shifts to search for (loss of HPO3 with gain of water, and H2 loss derivative of this)
v0.8:
- added feature to automatically extraction of csv (--extract_path)
v0.7:
- added new option: -samplelist. This can be used to automatically generate the merge files. The samplelist is anyways required for the jupyter notebook.
- merge and copy in one script (merge_copy.sh)
- small code improvements (using utf-8 instead of utf-8-sig)
v0.6:
- added a new label scheme for DEB + postlabel (based on theoretical mass)
v0.5:
- masses corrected and rounded to 6 digits according to http://ciaaw.org/atomic-masses.htm
- Bug-fix for windows machine - Generation of colon separated masses modified (modmasses)
- new labelling scheme added for single phosphate labelling (4.008 Da)
- For this the labelled Phosphate is added as a 'P ' within the sequence (e.g. UGCAUGPU)
- excluded generation of unlabelled nucleotide combinations when segmental labelling was used (any labelling scheme has to be used)
v0.4:
- bug-fix: unlabelled samples produced IndexError when naming Shift-folder.
v0.3:
- merging script automized partially
- new argument: samplecount
- added arguments for mzXML path, list file path and xQuest result folder path
- renaming and permission setting of merged_xquest.xml done in copy_result.sh
TODOs:
    - low/high resolution
    - job chaining
    -
"""



# Import modules
import argparse
import os
import numpy as np
import pandas as pd
import re
import shutil
import codecs
import csv
from pathlib import Path
from shutil import copyfile
from os import path
import sys
import json
from json import JSONEncoder

# Set up so this script can be called from the command line - first define functions and classes
class BaseRNA:
    """Class defining the required properties of a base in a protein-RNA cross-link"""
    def __init__(self, name, code, mass):
        self.code = name
        self.mass = mass
        self.code = code
        self.shift = 0

    def set_shift(self, shift):
        """Define the isotopic shift given by the labelling scheme"""
        self.shift = shift

    def set_mass(self, mass):
        """Define the unlabelled mass for the base in the labelling scheme"""
        self.mass = mass

    def set_name(self, name):
        """Define the name for the base in the labelling scheme"""
        self.name = name

# Set up so this script can be called from the command line - call in main function
def main():

    ########Production variables start here#########
    # Set version flag
    paramgen_ver = str(0.894)

    # Get variables from the command line
    parser = argparse.ArgumentParser(
        description='A script to generate the parameter files required to run a CLIR-MS xQuest search. One search is produced per mass shift. A set of shell scripts are produced that need to be edited in order to run the searches. The default files "xmm.def" and "xquest.def" must be present in tis directory first - if not, you can produce them with the command "runXquest.pl -getdef".')
    parser.add_argument('-RNAseq', '--RNAseq', required=True,
                        help='Sequence of the RNA of interest, using up to 5 bases (ACGUX, but must use a labelling scheme with the equivalent number of bases)')
    parser.add_argument('-projname', '--projname', required=True,
                        help='Name of the directory in which the search files will be created and run from')  
    parser.add_argument('-dbpath', '--dbpath', required=True,
                        help='The full path to the FASTA file to be searched against (including the file itself)')
    parser.add_argument('-dbcomm', '--dbcomm', required=False, default='',
                        help='Optional - a comment about the database to be added to the xQuest.def files')
    parser.add_argument('-labschem', '--labschem', required=False, default='',  choices=['Ribo', '4SU', 'Ribo_DEB', '13C15N', '15N', 'Ribo_Sing_U', 'Ribo_Sing_C', 'Ribo_Sing_G', 'Ribo_Sing_A', '18O_PNK', '18O_PNK_DEB', '18O_PNK_Sing', '18O_PNK_DEB', ''],
                        help='Optional - Labelling scheme used to label the RNA adduct (available options: Ribo, 4SU, Ribo_DEB, 13C15N, 15N, Ribo_Sing_U, Ribo_Sing_C, Ribo_Sing_G, Ribo_Sing_A, 18O_PNK, 18O_PNK_DEB, 18O_PNK_Sing, 18O_PNK_DEB, or leave out for unlabelled RNA')
    parser.add_argument('-maxlen', '--maxlen', required=False, default=4,
                        help='Optional - set to 4 or more to increase the maximum RNA length (up to the length of the full sequence)')
    parser.add_argument('-samplecount', '--samplecount', required=False, default=1,
                        help='Optional - Set the number of different conditions that were used in the experiment. An according number of merges will have to be performed later')
    parser.add_argument('-samplelist', '--samplelist', required=False, default='',
                        help='Optional - csv file containing a list of all mzXML files and the corresponding experiment (used for merging the files by experiment later). This option will override -samplecount and -listpath')
    parser.add_argument('-mzxmlpath', '--mzxmlpath', required=False, default='',
                        help='Optional - Full path to mzXML folder. If not used, mzXML path has to be edited manually in runxquestmulti.sh')
    # parser.add_argument('-listpath', '--listpath', required=False, default='',
                        # help='Optional - Full path to the list file, specifying all mzXML files for the analysis. If not used, list path has to be edited manually in runxquestmulti.sh')
    parser.add_argument('-xqresultpath', '--xqresultpath', required=False, default='',
                        help='Optional - Full path to the xQuest result folder. At the end of analysis the results will be copied there. If not used, xQuest result path has to be edited manually in copy_result.sh')
    parser.add_argument('-peponly', '--peponly', required=False, default='0',
                        help='Optional - If set to 1, will also generate the parameters/folders for a peptide only search (for transferred FDR)')
    parser.add_argument('-desktop', '--desktop', required=False, default='0',
                        help='Optional - If set to 1, will remove the "-euler" flag from xQuest jobs')
    # parser.add_argument('-custlabschem', '--custlabschem', required=False, default='',
    #                     help='Optional - specify a file containing a modified labelling scheme.')
    parser.add_argument('-custloss', '--custloss', required=False, default='',
                        help='Optional - specify a file containing a modified set of neutral loss products.')
    parser.add_argument('-mzxml', '--mzxml', required=False, default='',
                        help='Optional - specify "-mzxml c", as per with the core xQuest pipeline, if using centroided mzXML files with the extension .c.mzxml')

    args = parser.parse_args()

    # Set the RNA sequence:
    RNA_seq = args.RNAseq
    # Set a name for this project - this will be the name of the subdirectory into which everything is put
    projname = args.projname
    # Set the full path of your database file
    dbpath = args.dbpath
    dbcomment = args.dbcomm
    # Set the labelling scheme - TdV, AK, 4SU or MG
    labschem = args.labschem
    # RNA lengths to search for
    args.maxlen = int(args.maxlen)
    if labschem == '18O_PNK_Sing':
        args.maxlen += 1
    RNA_lengths = [i for i in range(1, len(RNA_seq) + 1) if i <= args.maxlen]
    # Set the number of different conditions / samples that should be merged in the end
    sample_count = int(args.samplecount)
    # Set the path to the mzXML folder
    mzxmlpath = args.mzxmlpath
    # Set the path to the list file
    samplelist = args.samplelist
    # Set the path to the list file
    # listpath = args.listpath
    # Set the path to the mzXML folder
    xqrespath = args.xqresultpath
    # Set the RNA sequence:
    desktop_status = args.desktop
    # Set the custom labelling schemes flag
    # custlabschem = args.custlabschem
    # print("Lab scheme flag: " + str(custlabschem))
    # Set the custom neutral losses flag
    custRNAloss = args.custloss
    # print("Loss scheme flag: " + str(custRNAloss))
    ########Production variables end here#########
    
    # Set the centroided mzXML flag, for use with .c.mzXML files (use to generate the pQuest command)
    cent_mzxml = args.mzxml
    if cent_mzxml == 'c':
        pq_mzxml_flag = '-mzxml c'
        print("Setting input file type as centroided mzXML with extension .c.mzXML .")
    else:
        pq_mzxml_flag = ' '

    #################################
    # Start checking the variables #
    #################################

    # Check that the FASTA file exists
    if path.isfile(dbpath) == False:
        print("Error (-dbpath): Database file path not valid. Check you have specified the correct path.")
        sys.exit(1)

    # Check for xmm.def
    if path.isfile('xmm.def') == False:
        print("Error: No xmm.def file present in this directory. Use \"rnxq_getfiles -def\" to generate initial files.")
        sys.exit(1)

    # Check for xquest.def
    if path.isfile('xquest.def') == False:
        print("Error: No xquest.def file present in this directory. Use \"rnxq_getfiles -def\" to generate initial files.")
        sys.exit(1)

    # Check mzxml path is a path
    if path.isdir(mzxmlpath) == False:
        print("Error (-mzxmlpath): Path to mzXML files is not a directory. Check you have specified the correct path.")
        sys.exit(1)

    # Check sample list exists
    if path.isfile(samplelist) == False:
        print("Error (-samplelist): Sample list file path not valid. Check you have specified the correct path.")
        sys.exit(1)
        
    # Check xq results directory exists
    if path.isdir(xqrespath) == False:
        print("Error (-xqresultpath): Path to xQuest results is not a directory. Check you have specified the correct path.")
        sys.exit(1)

    #############################
    # End of variable checking #
    #############################
    if samplelist != '':
        if sample_count != 1:
            print("Warning: -samplelist option used. Value of -sample_count will be ignored!")


    # List of bases to be used
    ribo_list = {
        "A": BaseRNA("Adenosine", "A", 347.063085), #C10H14N5O7P
        "C": BaseRNA("Cytidine", "C", 323.051851), #C9H14N3O8P
        "G": BaseRNA("Guanosine", "G", 363.057999), #C10H14N5O8P
        "U": BaseRNA("Uridine", "U", 324.035867), #C9H13N2O9P
        "X": BaseRNA("4-Thio-Uridine", "X", 306.025302), #C9H11N2O8P
        "P": BaseRNA("Single-Phosphate", "P", 18.010565) # H2O
    }
    ribo_bases = ribo_list.keys()

    # Set the output file name:
    outfilename = "CLIR_MS_mass_shifts_for_R.csv"
    fullmapfilename = "All_masses_shifts.csv"

    # Dev values for IDE
    # RNA_seq = "UGCAUGU"
    # projname = "Dcl5_PL"
    # dbpath = "/nfs/nas22.ethz.ch/fs2202/biol_imsb_aebersold_1/sarnowsc/Databases/FOX1_RRM.fasta"
    # dbcomment = "FOX1 supplied by AK"
    # labschem = "AK"
    # RNA_lengths = []
    # RNA_maxlen = 4
    # for i in range(1,len(RNA_seq)+1):
    #     RNA_lengths.append(i)
    # RNA_lengths = RNA_lengths[0:RNA_maxlen] #-1
    # End of dev values for IDE

    # total_shift and additional_mass are required for labschem == 18O_PNK, and allow for a chemical linker
    total_shift = 0
    additional_mass = 0
    # imported from loss 
    water_mass = 18.010565 #H2O
    phosphate_mass = 79.966331 #HPO3

    # Set RNA labelling scheme related parameters
    # RNA_labschems
    if labschem == "Ribo" or labschem == "4SU":
        # Assumes 13C at all atoms in the ribose
        for base in ribo_list.values():
            base.set_shift(5.016774) #Ch5-C5
        ribo_list["X"].set_shift(0)
    
    elif labschem == "Ribo_DEB":
        # 13C ribose, in combination with a DEB cross-linker
        for base in ribo_list.values():
            base.set_shift(5.016774) #Ch5-C5
        ribo_list["X"].set_shift(0)
        additional_mass = 86.089398
    
    elif labschem == "13C15N":
        # 13 C + 15 N metabolic labelling of in vitro transcribed RNA
        ribo_list["A"].set_shift(15.018723) #Ch10Nh5-C10N5
        ribo_list["C"].set_shift(12.021298) #Ch9Nh3-C9N3
        ribo_list["G"].set_shift(15.018723) #Ch10Nh5-C10N5
        ribo_list["U"].set_shift(11.024263) #Ch9Nh2-C9N2
        
    elif labschem == "15N":
        # 15 N metabolic labelling of in vitro transcribed RNA
        ribo_list["A"].set_shift(4.985175) #5*Nshift (0.997035)
        ribo_list["C"].set_shift(2.991105) #3*Nshift (0.997035)
        ribo_list["G"].set_shift(4.985175) #5*Nshift (0.997035)
        ribo_list["U"].set_shift(1.99407) #2*Nshift (0.997035)
        
    elif labschem == "Ribo_Sing_U":
         # For use in experiments where only one U is labelled - labelled U encoded as X
        for base in ribo_list.values():
            base.set_shift(0)
        ribo_list["X"].set_shift(5.016774) #Ch5-C5
        ribo_list["X"].set_mass(324.035867) #C9H13N2O9P
        ribo_list["X"].set_name("SingLab-U")
        
    elif labschem == "Ribo_Sing_G":
         # For use in experiments where only one G is labelled - labelled G encoded as X
        for base in ribo_list.values():
            base.set_shift(0)
        ribo_list["X"].set_shift(5.016774) #Ch5-C5
        ribo_list["X"].set_mass(363.057999) #C10H14N5O8P
        ribo_list["X"].set_name("SingLab-G")
        
    elif labschem == "Ribo_Sing_C":
         # For use in experiments where only one C is labelled - labelled C encoded as X
        for base in ribo_list.values():
            base.set_shift(0)
        ribo_list["X"].set_shift(5.016774) #Ch5-C5
        ribo_list["X"].set_mass(323.051851) #C9H14N3O8P
        ribo_list["X"].set_name("SingLab-C")
        
    elif labschem == "Ribo_Sing_A":
         # For use in experiments where only one A is labelled - labelled A encoded as X
        for base in ribo_list.values():
            base.set_shift(0)
        ribo_list["X"].set_shift(5.016774) #Ch5-C5
        ribo_list["X"].set_mass(347.063085) #C10H14N5O7P
        ribo_list["X"].set_name("SingLab-A")
        
    elif labschem == "18O_PNK":
        # No shifts on the nucleotides
        additional_mass = 79.966331 #HPO3
        total_shift = 6.012735 #Oh3-O3
        
    elif labschem == "18O_PNK_DEB":
        # No shifts on the nucleotides
        additional_mass = 166.055729 #HPO3 + DEB
        total_shift = 6.012735 #Oh3-O3
        
    elif labschem == "18O_PNK_SDA":
        # No shifts on the nucleotides
        additional_mass = 162.008196 #HPO3 + SDA_monolink
        total_shift = 6.012735 #Oh3-O3
    
    elif labschem == "18O_PNK_Sing":
        # No shifts on the nucleotides
        ribo_list["P"].set_shift(4.00849) #Oh2-O2
    
    elif labschem == "":
        # Print warning that other parameters need to be modified
        print("Warning: if no labelling scheme is specified, RNA without isotope labels is assumed.")
        print("The files \"xquest.def\" and \"xmm.def\" in the subfolder \"Shift_0\" must be further modified for a search without isotopic shifts.")
        print("Details can be found in Tables 1 and 2, Leitner, A., et al, Nat Protoc 9, 120–137 (2014)")

    # dictionary of neutral losses that are considered
    if custRNAloss != '':
        print("Using custom neutral loss dictionary file "+custRNAloss+".")
        with open(custRNAloss) as json_file: 
            neutloss_dic = json.load(json_file)
    else:
        neutloss_dic = {
            # You can add other masses here as we discover them :)
            "H2O": water_mass,  # This must always be here as it's used more widely
            "H4O": water_mass + 2.01565,
            "HPO3": phosphate_mass,  # H1O3P1
            "H3PO3": phosphate_mass + 2.01565,  # H1O3P1
            "H3PO4": phosphate_mass + water_mass,
            "H2": 2.01565,
            "HPO3+H2O": phosphate_mass - water_mass, #new modification found by 607/617 pair
            "H3PO3+H2O": phosphate_mass + 2.01565 - water_mass, #new modification above but H2 loss derivative
            "H4O2": 2*water_mass,
            "H5PO5": (2*water_mass) + phosphate_mass
        }

    # Generate a list of sequences in a sliding window
    seq_list = []
    for i in RNA_lengths:
        seq_list = seq_list + ([RNA_seq[j:j + i] for j in range(len(RNA_seq) - i + 1)])

    # Sort all string items in seq_list alphabetically, i.e. make UG into GU
    seq_list_sorted = []
    for i in seq_list:
        sorted_seq = ''.join(sorted(i))
        # Replace any AUX as sometimes this is "special" and cannot be a variable name
        sorted_seq.replace("AUX", "AXU")
        seq_list_sorted.append(sorted_seq)

    # Make the list unique
    seq_list_unique_with_null = list(set(seq_list_sorted))
    # Empty lists for calculated values
    mass_natural = []
    mass_shift = []

    # Count the base content in the unique sequences then the mass of this
    seq_list_unique = []
    for i in seq_list_unique_with_null:
        # For every sequence in the list, count its base content and multiply by the mass/shift
        shift = 0
        mass = 0
        for j in ribo_list.values():
            base_count = i.count(j.code)
            shift += j.shift * base_count
            mass += j.mass * base_count

        # total_shift and additional_mass are 0 except for MG-labelling scheme.
        shift += total_shift
        mass += additional_mass

        # subtract water for each additional nucleotide
        mass -= (len(i)-1) * water_mass
        if shift != 0.0 or labschem == '':
            if labschem == '18O_PNK_Sing':
                i = re.sub('P','',i)
            if len(i)>0:
                seq_list_unique.append(i)
                mass_natural.append(mass)
                mass_shift.append(shift)

    # Convert to numpy arrays for addition to make labelled values
    mass_natural = np.array(mass_natural)
    mass_shift = np.array(mass_shift)

    # Labelled is addition of natural and shift
    mass_labelled = mass_natural + mass_shift

    # Create arrays which the output files will be based upon
    out_natural = np.column_stack((seq_list_unique, mass_natural, (len(seq_list_unique) * ["Natural"])))
    out_labelled = np.column_stack((seq_list_unique, mass_labelled, (len(seq_list_unique) * ["Heavy"])))
    out_shift = np.column_stack((seq_list_unique, mass_shift, (len(seq_list_unique) * ["Shift"])))

    # The full list is for the output file, the to_search list is for the search and only contains the light masses
    fulltab_out = np.row_stack((out_natural, out_labelled, out_shift))

    to_search = np.column_stack((seq_list_unique, mass_natural, mass_shift, (len(seq_list_unique)*['Natural'])))
    for i in neutloss_dic.keys():
        # Logic implemented below:
        fulltab_out = np.row_stack((fulltab_out,
            np.column_stack((seq_list_unique, (mass_natural-neutloss_dic.get(i)), (len(seq_list_unique)*['Natural_-' + i]))),
            np.column_stack((seq_list_unique, (mass_labelled-neutloss_dic.get(i)), (len(seq_list_unique)*['Heavy_-' + i])))))
        # Slightly different logic because the mass shift is also required for filtering later in the script
        to_search = np.row_stack((to_search, np.column_stack((seq_list_unique, (mass_natural-neutloss_dic.get(i)), mass_shift, (len(seq_list_unique)*['Natural_-' + i])))))

    # Create a separate array for just the integer natural masses and shifts for the R lookup by eliminating nay lines which contain "Heavy"
    maptab_out = fulltab_out[['Heavy' not in x for x in fulltab_out[:, 2]]]

    # The rest of the code deals with directory creation, and writing the .def files
    # Record the current directory
    cwd = os.getcwd()
    # Use path to record what the project directory path will be - this ensures multi-OS compatibility
    cwd_plus_proj = Path(str(cwd) + "/" + projname)
    # Make the project directory
    try:
        os.mkdir(projname)
    except FileExistsError:
        pass
    
    # Write the parameters to the new project subdirectory
    # Change to the project directory 
    os.chdir(cwd_plus_proj)
    # Write arguments to a file
    with open('RNxq_params.txt', 'w') as f:
        json.dump(args.__dict__, f, indent=2)
        f.write("\n#Generated using version "+ paramgen_ver)
    # Change back
    os.chdir(cwd)

    # Get the unique number of shifts, i.e. number of searches
    unique_shifts = list(set(mass_shift))
    # Convert the array to a Pandas data frame for easier manipulation, and set types to float
    to_search = pd.DataFrame(
        {'RNA_seq': to_search[:, 0], 'Mass': to_search[:, 1], 'Shift': to_search[:, 2], 'Type': to_search[:, 3]})
    to_search['Shift'] = to_search['Shift'].astype(float)
    to_search['Mass'] = to_search['Mass'].astype(float)
    to_search['Seq_Type'] = to_search[['RNA_seq', 'Type']].apply(lambda x: '_'.join(x), axis=1)

    # For each shift - creates the directories and definition files for XL searches (peptide only searches are defined separately below)
    for i in unique_shifts:
        os.chdir(cwd)
        # Create a DF containing just the rows with a shift equal to i
        to_search_filt = to_search[to_search['Shift'] == i]
        # Create a DF containing just the mod names/masses/neutral loss masses for that shift
        modnames = to_search_filt["Seq_Type"].values.tolist()
        modmasses = ",".join([str(x) for x in to_search_filt['Mass'].tolist()])
        # Create a less precise version of the shift (i.e. 2 dp) that can be used in the folder name
        try:
            int_shift = "Shift_" + (i.astype(int)).astype(str) + "_" + (i.astype(str).split("."))[1][0:2]
        except IndexError:
            # In case of no shift searches:
            int_shift = "Shift_0"
        # Create a path including the shift
        cwd_plus_proj_shift = Path(str(cwd_plus_proj) + "/" + int_shift)
        # Set the path where the template .def files can be collected from
        wd_xq = Path(cwd + "/" + "xquest.def")
        wd_xm = Path(cwd + "/" + "xmm.def")

        # Change to the project directory and create the shift directory (one per search)
        os.chdir(cwd_plus_proj)
        try:
            os.mkdir(int_shift)
        except FileExistsError:
            pass
        # Change to the shift directory and copy the .def template files
        os.chdir(int_shift)
        shutil.copy(wd_xq, cwd_plus_proj_shift)
        shutil.copy(wd_xm, cwd_plus_proj_shift)

        # Read in the xmm file
        with open('xmm.def', 'r') as file:
            filedata = file.read()
        # Replace the target string
        filedata = filedata.replace('<iso_shift>', i.astype(str)) # i is for each shift
        # All other parameters are left from the master definition files copeid at the start of the RNA pipeline
        
        # Write the file out again
        with open('xmm.def', 'w') as file:
            file.write(filedata)

            # Read in the xquest.def file
        with open('xquest.def', 'r') as file:
            filedata = file.read()
            # Replace the target string
        filedata = filedata.replace('<iso_shift>', i.astype(str)) # i is for each shift
        filedata = filedata.replace('<modname>', 'Shifts set for '+(','.join(modnames)))
        filedata = filedata.replace('<modmasses>', modmasses)
        filedata = filedata.replace('<dbpath>', dbpath)
        filedata = filedata.replace('<dbcomment>', dbcomment)
        # All other parameters are left from the master definition files copeid at the start of the RNA pipeline

        # Write the file out again
        with open('xquest.def', 'w') as file:
            file.write(filedata)

            # End the loop back in the main projetc directory
        os.chdir(cwd)

    # Set the directories and parameter files for peptide only search (only if enabled in parameters)
    if args.peponly =='1':
        os.chdir(cwd)
        # Create a path for the peptide only search
        cwd_plus_proj_shift = Path(str(cwd_plus_proj) + "/" + "Shift_0_PepOnly")
        # Set the path where the template .def files can be collected from
        wd_xq = Path(cwd + "/" + "xquest.def")
        wd_xm = Path(cwd + "/" + "xmm.def")

        # Change to the project directory and create the shift directory (one per search)
        os.chdir(cwd_plus_proj)
        try:
            os.mkdir("Shift_0_PepOnly")
        except FileExistsError:
            pass
        # Change to the shift directory and copy the .def template files
        os.chdir("Shift_0_PepOnly")
        shutil.copy(wd_xq, cwd_plus_proj_shift)
        shutil.copy(wd_xm, cwd_plus_proj_shift)
        
        # Print warning that other parameters need to be modified
        print("Warning: peptide-only search enabled.")
        print("The files \"xquest.def\" and \"xmm.def\" in the subfolder \"Shift_0_PepOnly\" must be further manually modified for a search without isotopic shifts.")
        print("Details can be found in Tables 1 and 2, Leitner, A., et al, Nat Protoc 9, 120–137 (2014)")

        # Read in the xmm file
        with open('xmm.def', 'r') as file:
            filedata = file.read()
        # Replace the target strings
        filedata = filedata.replace('<iso_shift>', '0') #as per protocols paper for shift 0 search
        # All other parameters are inherited from the default .def files

        # Write the file out again
        with open('xmm.def', 'w') as file:
            file.write(filedata)

        # Read in the xquest.def file
        with open('xquest.def', 'r') as file:
            filedata = file.read()
          
        # Replace the target string
        filedata = filedata.replace('<iso_shift>', '0') # i is for each shift
        filedata = filedata.replace('<modname>', 'Peptide only search')
        filedata = filedata.replace('<modmasses>', '0')
        filedata = filedata.replace('<dbpath>', dbpath)
        filedata = filedata.replace('<dbcomment>', dbcomment)
        # All other parameters are left from the master definition files copeid at the start of the RNA pipeline
        
        # Write the file out again
        with open('xquest.def', 'w') as file:
            file.write(filedata)

        # End 9back in the main projetc directory
        os.chdir(cwd)

    # Write shell scripts to run the search - encoding is Unix type so dos2unix is not required
    # Each of these now takes the form of a loop based on the directories inside the project directory
    os.chdir(cwd_plus_proj)
    shift_dirs = list(filter(os.path.isdir, os.listdir(os.getcwd())))

    ## read samplelist for generating merge files.
    if samplelist != '':
        # load samplelist into dataframe
        files_df = pd.read_csv(samplelist, names=['filename','experiment'], skip_blank_lines=True)
        # make the experiment text rather than a number, and strip whitespace (left and right) and replace other spaces with underscore
        files_df['experiment'] = files_df['experiment'].astype(str)
        files_df['experiment'] = files_df['experiment'].str.strip()
        files_df['experiment'] = files_df['experiment'].str.replace(' ', '_')
        
        # Check if any of the experiment names are numeric only, and exit if so
        contains_nums = pd.to_numeric(files_df['experiment'], errors='coerce').notnull().unique()
        if True in contains_nums:
            print("Error (-samplelist): Some of the experiment names in the list file are numeric - they must contain some text (i.e. if using \"100\", try using \"_100\" instead.")
            sys.exit(1)
        else:
            print ("Checked sample type names - all are valid.")
        
        # generate fullpath of files and strip whitespace
        files_df['filename'] = files_df['filename'].str.strip()
        files_df['fullpath'] = files_df.filename + "/" + files_df.filename + "_matched"
        # get a list of unique experiment identifiers
        samples = list(set(files_df.experiment))
        sample_count = len(samples)
        # generate individual merge files
        for i, sample in enumerate(samples):
            with codecs.open("merge_"+str(i+1), "w", "utf-8") as merge_single:
                for entry in files_df[files_df.experiment == sample].fullpath:
                    merge_single.write(entry + "\n")
        # Generated by default from v 0.88 to declutter inputs
        # if listpath == '': ## only works from LINUX!!
        listpath = mzxmlpath+"toSearch_"+projname
        print("listpath set to "+ listpath + ".")
        with codecs.open(listpath, "w", "utf-8") as toSearch:
            toSearch.writelines("\n".join(list(files_df.filename)))

    with codecs.open("runxqmulti.sh", "w", "utf-8") as runxqmulti:
        L = ["#!/bin/sh\n", "#Define the variables for list of mzxml files and where they are located \n", "LIST="+listpath+" \n",
             "MZXML_PATH="+mzxmlpath+" \n"]
        runxqmulti.writelines(L)
        #Add custom lines to find the full path of the perl scripts in any Unix env
        L = ["#To call from Python, xQuest must be run from the absolute path:"+" \n", "XQIN=`whereis runXquest.pl` \n", "FULLXQ=`awk -F\': \' \'{print $2}\' <<< $XQIN` \n",
             "PQIN=`whereis pQuest.pl` \n", "FULLPQ=`awk -F\': \' \'{print $2}\' <<< $PQIN` \n"]
        runxqmulti.writelines(L)
        runxqmulti.write('for SEARCHDIR in ' + ' '.join(shift_dirs) + '\n')
        L = ['do \n', '\t cd $SEARCHDIR \n', '\t $FULLPQ -list $LIST -path $MZXML_PATH ', pq_mzxml_flag, ' \n', '\t wait & \n',
             '\t $FULLXQ -list $LIST -xmlmode -pseudosh -euler \n', '\t wait & \n',
             '\t echo Search for $SEARCHDIR is submitted \n', '\t cd .. \n', 'done \n']
        # If desktop, remove Euler flag
        if desktop_status=='1':
            L2 = []
            for string in L:
                newstring = string.replace("-euler","")
                L2.append(newstring)
            L=L2
        runxqmulti.writelines(L)
        runxqmulti.close()

    # Write merger file
    with codecs.open("merge_copy.sh", "w", "utf-8") as merge_copy:
        L = ["#!/bin/sh\n", "#Set the pattern name for the beginning of all result folders \n", "RESULT="+projname+" \n"]
        merge_copy.writelines(L)
        L = ["MGIN=`whereis mergexml.pl` \n", "FULLMG=`awk -F\': \' \'{print $2}\' <<< $MGIN` \n"]
        merge_copy.writelines(L)
        if samplelist != '':
            for i, sample in enumerate(samples):
                merge_copy.write("TYPE"+str(i+1)+"=" + str(sample) + "\n")
        else:
            for i in range(1, sample_count+1):
                merge_copy.write("TYPE"+str(i)+"=  \n")
        merge_copy.write('for SEARCHDIR in ' + ' '.join(shift_dirs) + '\n')
        L = ['do \n', '\t cd $SEARCHDIR \n']
        merge_copy.writelines(L)

        for i in range(1, sample_count+1):
            L = ['\t $FULLMG -list ../merge_'+str(i)+' -resdir "${RESULT}${TYPE'+str(i)+'}_${SEARCHDIR}" -v \n', '\t wait & \n']
            merge_copy.writelines(L)

        L = ['\t echo Merges for $SEARCHDIR complete. \n', '\t cd .. \n', 'done \n']
        merge_copy.writelines(L)
       
        # Write line related to the XML extraction script
        L = ['echo extracting data to .xQuest.csv \n','bsub rnxq_extractxml -exp $RESULT -list ',samplelist,' -fasta ', dbpath,' \n']
        # If desktop, remove bsub flag
        if desktop_status=='1':
            L2 = []
            for string in L:
                newstring = string.replace("bsub ","")
                L2.append(newstring)
            L=L2
        merge_copy.writelines(L)

        L = ["#Set the pattern name for the beginning of all result folders \n", "RESULT="+projname+"* \n", "XQRES="+xqrespath+" \n"]
        merge_copy.writelines(L)
        merge_copy.write('for SEARCHDIR in ' + ' '.join(shift_dirs) + '\n')
        L = ['do \n', '\t cd $SEARCHDIR \n', '\t cp -R ./${RESULT} $XQRES \n', '\t wait & \n', '\t rm -rf "${RESULT}" \n',
             '\t wait & \n', '\t echo $SEARCHDIR search result is copied \n', '\t cd .. \n', 'done \n',
             'echo renaming all merged_xquest.xml to xquest.xml \n', 'find $XQRES/$RESULT -iname "merged_xquest.xml"',
             " -exec rename merged_xquest.xml xquest.xml '{}' \; \n",
             "echo setting permissions to allow access with xQuestviewer \n", "chmod -R 777 $XQRES/$RESULT \n"]

        merge_copy.writelines(L)
        merge_copy.close()
        
    # Write xq.progress running file
    with codecs.open("xqprog.sh", "w", "utf-8") as xqprog:
        L = ["#!/bin/sh\n", "#Set the pattern name for the beginning of all result folders \n", "PROGIN=`whereis xq.progress`"+" \n",
        "FULLPROG=`awk -F': ' '{print $2}' <<< $PROGIN`"+" \n",
        "$FULLPROG"    ]
        xqprog.writelines(L)
        xqprog.close()

    os.chdir(cwd_plus_proj)
    # Write mapping file
    np.savetxt(outfilename, maptab_out, fmt='%s', delimiter=',', comments='', header='modification,mass,loss_type')
    np.savetxt(fullmapfilename, fulltab_out, fmt='%s', delimiter=',', comments='', header='modification,mass,loss_type')
    os.chdir(cwd)

if __name__ == "__main__": 
    # calling the main function 
    main() 
