:: Starts the jupyter notebook (needs to be personalized!)

:: press win and search for 'anaconda prompt'. 
:: right click and go to the original location.
:: right click the link and go to properties.
:: copy the target link and paste it here. 
:: at the end of the line before the quotation mark insert:
:: ; jupyter notebook
::                                                                                                          \/ Conda hook path here \/                           \/ Conda root here \/
%windir%\System32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy ByPass -NoExit -Command "& 'C:\Anaconda3\shell\condabin\conda-hook.ps1' ; conda activate 'C:\Anaconda3'; jupyter notebook"
